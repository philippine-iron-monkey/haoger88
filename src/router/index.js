// 导入 vue 库
import Vue from 'vue'
// 导入 路由管理器
import VueRouter from 'vue-router'
// 导入 另一个组件
import HomeView from '../views/HomeView.vue'

// 注册 vue 全局“组件”
//      router-link
//      router-view
Vue.use(VueRouter)

// 准备 “路由表”
const routes = [
  // 路由信息对象
  {
    path: '/',  // hash 路径
    name: 'home',  // 路由名字
    component: HomeView  // HomeView组件对象
  },
]

// 创建 路由管理器对象实例
const router = new VueRouter({
  routes
})

// 导出了路由管理器对象实例
export default router
