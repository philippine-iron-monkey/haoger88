// main.js
// 导入 Vue 函数
import Vue from 'vue'
// import App from './App.vue'  // 单文件组件
import App from '../src/04_动态class.vue'




// 导入 自定义的各种代码文件

import router from './router'  // 路由管理器
import store from './store'  // vuex状态管理器

// 关闭生产环境提示
Vue.config.productionTip = false

// 方式一：全局-过滤器
// 任意的 .vue文件内"直接"使用
// 语法：Vue.filter（“过滤器名”，值=> 处理结果）
// Vue.filter("reverse", val => split("").severse().join())

// Vue 全局过滤器：可以在整个项目中所有组件里使用
Vue.filter("reverse", (val, s) => {
  return val.split("").reverse().join(s)
})

new Vue({
  render: h => h(App),
}).$mount('#app')